#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <sys/stat.h>
#include <jpeglib.h>


int * readImageToRGBStream(int argc, char *argv[])
{
	int rc, i, j;
	char *syslog_prefix = (char *)malloc(1024);
	sprintf(syslog_prefix, "%s", argv[0]);
	openlog(syslog_prefix, LOG_PERROR | LOG_PID, LOG_USER);
	if (argc != 2)
	{
		fprintf(stderr, "USAGE: %s filename.jpg\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	struct stat file_info;
	unsigned long jpg_size;
	unsigned char *jpg_buffer;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	unsigned long bmp_size;
	unsigned char *bmp_buffer;
	int row_stride, width, height, pixel_size;
	rc = stat(argv[1], &file_info);
	if (rc)
	{
		syslog(LOG_ERR, "FAILED to stat source jpg");
		exit(EXIT_FAILURE);
	}
	jpg_size = file_info.st_size;
	jpg_buffer = (unsigned char *)malloc(jpg_size + 100);
	int fd = open(argv[1], O_RDONLY);
	i = 0;
	while (i < jpg_size)
	{
		rc = read(fd, jpg_buffer + i, jpg_size - i);
		syslog(LOG_INFO, "Input: Read %d/%lu bytes", rc, jpg_size - i);
		i += rc;
	}
	close(fd);
	syslog(LOG_INFO, "Proc: Create Decompress struct");
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);
	syslog(LOG_INFO, "Proc: Set memory buffer as source");
	jpeg_mem_src(&cinfo, jpg_buffer, jpg_size);
	syslog(LOG_INFO, "Proc: Read the JPEG header");
	rc = jpeg_read_header(&cinfo, TRUE);
	if (rc != 1)
	{
		syslog(LOG_ERR, "File does not seem to be a normal JPEG");
		exit(EXIT_FAILURE);
	}
	syslog(LOG_INFO, "Proc: Initiate JPEG decompression");
	jpeg_start_decompress(&cinfo);
	width = cinfo.output_width;
	height = cinfo.output_height;
	pixel_size = cinfo.output_components;
	syslog(LOG_INFO, "Proc: Image is %d by %d with %d components", width, height, pixel_size);
	bmp_size = width * height * pixel_size;
	bmp_buffer = (unsigned char *)malloc(bmp_size);
	row_stride = width * pixel_size;
	syslog(LOG_INFO, "Proc: Start reading scanlines");

	int totalRGBstream = width*height*3;
	syslog(LOG_INFO, "Proc: total RGB elements in stream: %d", totalRGBstream);
	int *rgbPixels = (int*)malloc(width*height*sizeof(int)*3);
	int sizeOfMalloc = width*height*sizeof(int)*3;
	syslog(LOG_INFO, "Proc: Assigned int array malloc size: %d", sizeOfMalloc);
	
	syslog(LOG_INFO, "aaaaaaa");

	// rgbPixels = RGBRGBRGBRGBRGBRGB...
	int atPixel = 0;
	syslog(LOG_INFO, "aaaaaaa2");

	while (cinfo.output_scanline < cinfo.output_height)
	{
		syslog(LOG_INFO, "aaaaaaa3");

		unsigned char *buffer_array[2];
		syslog(LOG_INFO, "aaaaaaa4");

		jpeg_read_scanlines(&cinfo, buffer_array, 1);
		syslog(LOG_INFO, "aaaaaaa5");

		// get the pointer to the row:
		unsigned char *pixel_row = (unsigned char *)(buffer_array[0]);
		// iterate over the pixels:
		for (int i = 0; i < cinfo.output_width; i++)
		{
			// convert the RGB values to a float in the range 0 - 1
			int red = (*pixel_row++);
			int green = (*pixel_row++);
			int blue = (*pixel_row++);
			syslog(LOG_INFO, "Proc: %d %d %d", red,green,blue);
			rgbPixels[atPixel] = red;
			rgbPixels[atPixel+1] = green;
			rgbPixels[atPixel+2] = blue;
			atPixel = atPixel+3;
		}
	}

	syslog(LOG_INFO, "Proc: Done reading scanlines");
	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	free(jpg_buffer);
	fd = open("output.ppm", O_CREAT | O_WRONLY, 0666);
	char buf[1024];
	rc = sprintf(buf, "P6 %d %d 255\n", width, height);
	write(fd, buf, rc);				 // Write the PPM image header before data
	write(fd, bmp_buffer, bmp_size); // Write out all RGB pixel data
	close(fd);
	free(bmp_buffer);
	syslog(LOG_INFO, "End of decompression");
	int * rgbData[3];
	rgbData[0] = rgbPixels;
	rgbData[1] = &width;
	rgbData[2] = &height;
	printf("%d", rgbData[0][0]);
	return rgbData[0];
}


int main(int argc, char *argv[])
{
	int *headerRGBStream;
	syslog(LOG_INFO, "Starting");
	headerRGBStream = readImageToRGBStream(argc,argv);
}

